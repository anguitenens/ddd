package edu.ddd.auction.application;

import edu.ddd.auction.domain.*;
import edu.ddd.auction.domain.auction.Auction;
import edu.ddd.auction.domain.auction.AuctionId;
import edu.ddd.auction.domain.auction.AuctionRepository;
import edu.ddd.auction.domain.bid.AuctionBidRevoked;
import edu.ddd.auction.domain.bid.Bid;
import edu.ddd.auction.domain.bid.BidId;
import edu.ddd.auction.domain.bid.BidRepository;
import edu.ddd.auction.domain.buyer.Buyer;
import edu.ddd.auction.domain.buyer.BuyerId;
import edu.ddd.auction.domain.buyer.BuyerRepository;
import edu.ddd.auction.domain.item.Item;
import edu.ddd.auction.domain.item.ItemId;
import edu.ddd.auction.domain.item.ItemRepository;
import edu.ddd.auction.domain.seller.Seller;
import edu.ddd.auction.domain.seller.SellerId;
import edu.ddd.auction.domain.seller.SellerRepository;

import java.util.Date;
import java.util.List;

public class AuctionService {

    BidRepository bidRepository;
    AuctionRepository auctionRepository;
    ItemRepository itemRepository;
    SellerRepository sellerRepository;
    EventPublisher eventPublisher;
    BuyerRepository buyerRepository;


    public void announce(String sellerId, String itemId,
                         long initialPrice, Date closeDate) {
        SellerId sid = new SellerId(sellerId);
        Seller seller = sellerRepository.getById(sid);

        ItemId iid = new ItemId(itemId);
        Item item = itemRepository.getById(iid);


        AuctionId aid = auctionRepository.getNextId();

        Auction auction = seller.announceAuction(
                aid,item,initialPrice,closeDate);

        auctionRepository.save(auction);

        eventPublisher.propagate();
    }

    public void makeBid(String auctionId, String buyerId,
                        long price) {
        AuctionId aid = new AuctionId(auctionId);
        Auction auction = auctionRepository.getById(aid);

        BuyerId buyerId1 = new BuyerId(buyerId);
        Buyer buyer = buyerRepository.getById(buyerId1);

        BidId bidId = bidRepository.getNextId();
        auction.registerBid(bidId, buyer, price);

        eventPublisher.propagate();
    }

    public void handleBidChanged(AuctionBidRevoked event) {
        AuctionId auctionId=new AuctionId(event.getAuctionId());
        Auction auction = auctionRepository.getById(auctionId);
        List<Bid> allBids = bidRepository.getAllByAuctionId(auctionId);

        Bid maxBid = null;
        for (Bid bid : allBids ){
            if (maxBid==null){
                maxBid = bid;
            }
            if (bid.getPrice() > maxBid.getPrice()) {
                maxBid = bid;
            }
        }

        auction.changeHighestBid(maxBid);
    }

    public void revokeBid(String bidId) {
        BidId bidId1 = new BidId(bidId);
        Bid bid = bidRepository.getById(bidId1);
        Auction auction = auctionRepository.
                getById(bid.getAuctionId());
        bid.revoke();


        eventPublisher.propagate();
    }

    public void close(String auctionId) {
        AuctionId aid = new AuctionId(auctionId);
        Auction auction = auctionRepository.getById(aid);

        auction.close();

        eventPublisher.propagate();
    }
}
