package edu.ddd.auction.domain.seller;

public class SellerId {

    private String id;

    public SellerId(String aid) {
        this.id = aid;
    }

    @Override
    public String toString() {
        return id;
    }
}
