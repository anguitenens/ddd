package edu.ddd.auction.domain.seller;

import edu.ddd.auction.domain.EventPublisher;
import edu.ddd.auction.domain.auction.Auction;
import edu.ddd.auction.domain.auction.AuctionAnnounced;
import edu.ddd.auction.domain.auction.AuctionId;
import edu.ddd.auction.domain.item.Item;

import java.util.Date;

public class Seller {

    SellerId sellerId;
    EventPublisher eventPublisher;


    Seller(SellerId sellerId, EventPublisher eventPublisher) {
        this.sellerId = sellerId;
        this.eventPublisher = eventPublisher;
    }

    public Auction announceAuction(AuctionId aid, Item item,
                                   long initialPrice, Date closeDate) {
        Auction auction = new Auction(aid, sellerId, item.getId(),
                initialPrice, closeDate, eventPublisher);
        eventPublisher.publish(new AuctionAnnounced(
                aid, sellerId, item.getId(), initialPrice, closeDate));
        return auction;
    }
}
