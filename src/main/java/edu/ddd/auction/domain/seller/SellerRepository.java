package edu.ddd.auction.domain.seller;

public interface SellerRepository {

    Seller getById(SellerId sellerId);
}
