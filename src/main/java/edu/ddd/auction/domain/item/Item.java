package edu.ddd.auction.domain.item;

public class Item {

    private ItemId itemId;

    Item(ItemId itemId) {
        this.itemId = itemId;
    }

    public ItemId getId() {
        return itemId;
    }
}
