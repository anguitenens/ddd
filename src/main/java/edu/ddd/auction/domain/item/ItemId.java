package edu.ddd.auction.domain.item;

public class ItemId {

    private String id;

    public ItemId(String aid) {
        this.id = aid;
    }

    @Override
    public String toString() {
        return id;
    }
}
