package edu.ddd.auction.domain.item;

public interface ItemRepository {

    Item getById(ItemId itemId);
}
