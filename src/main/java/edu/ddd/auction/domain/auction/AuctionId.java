package edu.ddd.auction.domain.auction;

public class AuctionId {

    private String id;

    public AuctionId(String aid) {
        this.id = aid;
    }

    @Override
    public String toString() {
        return id;
    }
}
