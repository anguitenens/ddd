package edu.ddd.auction.domain.auction;

import edu.ddd.auction.domain.Event;
import edu.ddd.auction.domain.item.ItemId;
import edu.ddd.auction.domain.seller.SellerId;

import java.util.Date;

public class AuctionAnnounced implements Event {

    private AuctionId auctionId;
    private SellerId sellerId;
    private ItemId itemId;
    private long initialPrice;
    private Date closeDate;

    public AuctionAnnounced(AuctionId auctionId, SellerId sellerId,
                     ItemId itemId,
                     long initialPrice, Date closeDate) {
        this.auctionId = auctionId;
        this.sellerId = sellerId;
        this.itemId = itemId;
        this.initialPrice = initialPrice;
        this.closeDate = closeDate;
    }
}
