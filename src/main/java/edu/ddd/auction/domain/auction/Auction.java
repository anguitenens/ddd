package edu.ddd.auction.domain.auction;

import edu.ddd.auction.domain.EventPublisher;
import edu.ddd.auction.domain.bid.*;
import edu.ddd.auction.domain.buyer.Buyer;
import edu.ddd.auction.domain.item.ItemId;
import edu.ddd.auction.domain.seller.SellerId;

import java.util.Date;

public class Auction {

    private AuctionId auctionId;
    private SellerId sellerId;
    private ItemId itemId;
    private long initialPrice;
    private Date closeDate;
    private HighestBid highestBid;
    private EventPublisher eventPublisher;
    private boolean active;

    public Auction(AuctionId auctionId, SellerId sellerId,
                   ItemId itemId,
                   long initialPrice, Date closeDate,
                   EventPublisher eventPublisher) {
        this.auctionId = auctionId;
        this.closeDate = closeDate;
        this.initialPrice = initialPrice;
        this.itemId = itemId;
        this.sellerId = sellerId;
        this.highestBid = null;
        this.eventPublisher = eventPublisher;
        this.active = true;
    }

    public Bid registerBid(BidId bidId, Buyer buyer, long currentPrice) {
        if (currentPrice <= getHighestPrice()) {
            throw new RuntimeException("entered bid less then current bid");
        }
        Bid bid = new Bid(bidId, auctionId,
                buyer.getBuyerId(), currentPrice);
        eventPublisher.publish(new AuctionBidRegistered(
                auctionId.toString(), bidId.toString(),
                buyer.getBuyerId().toString(), currentPrice
        ));
        return bid;
    }

    private long getHighestPrice() {
        if (highestBid == null) {
            return initialPrice;
        }
        return highestBid.getPrice();
    }


    public void close() {
        eventPublisher.publish(new AuctionBidClosed(
                auctionId.toString(), highestBid.getBidId().toString(),
                highestBid.getBuyerId().toString(), getHighestPrice()
        ));
        this.active = false;
    }

    public void changeHighestBid(Bid bid) {
        this.highestBid = new HighestBid(bid.getBuyerId(), bid.getBidId(), bid.getPrice());
    }
}
