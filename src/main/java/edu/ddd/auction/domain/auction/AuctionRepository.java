package edu.ddd.auction.domain.auction;

public interface AuctionRepository {

    AuctionId getNextId();

    Auction getById(AuctionId auctionId);

    void save(Auction auction);
}
