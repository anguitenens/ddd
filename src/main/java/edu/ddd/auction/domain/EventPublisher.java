package edu.ddd.auction.domain;

public interface EventPublisher {

    void publish(Event event);

    void propagate();
}
