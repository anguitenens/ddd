package edu.ddd.auction.domain.bid;

import edu.ddd.auction.domain.Event;

public class AuctionBidRegistered implements Event {


    private String auctionId;
    private String bidId;
    private String buyerId;
    private long price;

    public AuctionBidRegistered(String auctionId, String bidId, String buyerId, long price) {
        this.auctionId = auctionId;
        this.bidId = bidId;
        this.buyerId = buyerId;
        this.price = price;
    }
}
