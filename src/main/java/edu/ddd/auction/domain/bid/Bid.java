package edu.ddd.auction.domain.bid;

import edu.ddd.auction.domain.EventPublisher;
import edu.ddd.auction.domain.buyer.BuyerId;
import edu.ddd.auction.domain.auction.AuctionId;

public class Bid {

    private BidId bidId;
    private AuctionId auctionId;
    private BuyerId buyerId;
    private long price;
    private boolean revoked;
    private EventPublisher eventPublisher;

    public Bid(BidId bidId, AuctionId auctionId, BuyerId buyerId,
               long price) {
        this.bidId = bidId;
        this.auctionId = auctionId;
        this.buyerId = buyerId;
        this.price = price;
        this.revoked = false;
    }


    public AuctionId getAuctionId() {
        return auctionId;
    }

    public void revoke() {
        this.revoked = true;
        eventPublisher.publish(new AuctionBidRevoked(
                auctionId.toString(), bidId.toString(),
                buyerId.toString(), price
        ));
    }

    public BidId getBidId() {
        return bidId;
    }

    public BuyerId getBuyerId() {
        return buyerId;
    }

    public long getPrice() {
        return price;
    }

    public boolean isRevoked() {
        return revoked;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }
}
