package edu.ddd.auction.domain.bid;

import edu.ddd.auction.domain.Event;

public class AuctionBidClosed implements Event {
    private String auctionId;
    private String bidId;
    private String buyerId;
    private long price;


    public AuctionBidClosed(String auctionId, String bidId,
                            String buyerId, long highestPrice) {
        this.auctionId = auctionId;
        this.bidId = bidId;
        this.buyerId = buyerId;
        this.price = highestPrice;
    }


    public String getAuctionId() {
        return auctionId;
    }

    public String getBidId() {
        return bidId;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public long getPrice() {
        return price;
    }
}
