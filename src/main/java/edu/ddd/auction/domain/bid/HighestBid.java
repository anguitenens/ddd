package edu.ddd.auction.domain.bid;

import edu.ddd.auction.domain.buyer.BuyerId;

public class HighestBid {

    private BuyerId buyerId;
    private BidId bidId;
    private long price;

    public HighestBid(BuyerId buyerId, BidId bidId, long price) {
        this.bidId = bidId;
        this.buyerId = buyerId;
        this.price = price;
    }

    public long getPrice() {
        return this.price;
    }

    public BuyerId getBuyerId() {
        return buyerId;
    }

    public BidId getBidId() {
        return bidId;
    }

}
