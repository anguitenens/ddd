package edu.ddd.auction.domain.bid;

import edu.ddd.auction.domain.auction.AuctionId;

import java.util.List;

public interface BidRepository {

    BidId getNextId();

    Bid getById(BidId bidId);

    void save(Bid bid);

    List<Bid> getAllByAuctionId(AuctionId auctionId);
}
