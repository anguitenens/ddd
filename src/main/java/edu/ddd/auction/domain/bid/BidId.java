package edu.ddd.auction.domain.bid;

public class BidId {

    private String id;

    public BidId(String aid) {
        this.id = aid;
    }

    @Override
    public String toString() {
        return id;
    }
}
