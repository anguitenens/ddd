package edu.ddd.auction.domain.buyer;

public class Buyer {

    private BuyerId buyerId;

    public Buyer(BuyerId buyerId) {
        this.buyerId = buyerId;
    }

    public BuyerId getBuyerId() {
        return buyerId;
    }
}
