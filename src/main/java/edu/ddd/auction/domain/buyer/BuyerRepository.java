package edu.ddd.auction.domain.buyer;

public interface BuyerRepository {

    Buyer getById(BuyerId buyerId);
}
