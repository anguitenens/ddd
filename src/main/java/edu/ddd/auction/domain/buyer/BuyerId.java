package edu.ddd.auction.domain.buyer;

public class BuyerId {

    private String id;

    public BuyerId(String aid) {
        this.id = aid;
    }

    @Override
    public String toString() {
        return id;
    }
}
